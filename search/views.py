from django.views.generic import ListView
# noinspection PyUnresolvedReferences
from products.models import Product


class SearchProductView(ListView):
    template_name = "search/view.html"

    def get_queryset(self, *args, **kwargs):
        request = self.request
        search_query = request.GET.get('q')
        try:
            length = len(search_query)
        except:
            length = 0
        if search_query is not None and length != 0:
            return Product.objects.search(query=search_query)
        else:
            return Product.objects.filter(featured=True)

from django.http import JsonResponse, HttpResponse
from django.shortcuts import render, redirect
from .forms import ContactForm
from django.contrib.auth import login, authenticate, get_user_model


def home_page(request):
    context = {
        'title': 'Hello World',
        'content': 'Welcome to Home Page'
    }
    if request.user.is_authenticated:
        context["premium_content"] = "premium content enabled"
    return render(request, "home_page.html", context)


def about_page(request):
    context = {
        'title': 'About Us',
        'content': 'Welcome to About Page'
    }
    return render(request, "home_page.html", context)


def contact_page(request):
    contact_form = ContactForm(request.POST or None)
    context = {
        'title': 'Contact Us',
        'content': 'Welcome to Contact Page',
        'form': contact_form
    }
    if contact_form.is_valid():
        if request.is_ajax():
            return JsonResponse({"message": "Thank You"})
    if contact_form.errors:
        errors = contact_form.errors.as_json()
        if request.is_ajax():
            return HttpResponse(errors, status=400, content_type="application/json")
    return render(request, "contact/view.html", context)

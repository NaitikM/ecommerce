# base settings
from .base import *

# production config
from .production import *

# local config
try:
    from .local import *
except Exception as e:
    pass

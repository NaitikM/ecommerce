from django import forms
from django.contrib.auth import get_user_model
import re

User = get_user_model()
regex = '^[a-z0-9]+[\._]?[a-z0-9]+[@]\w+[.]\w{2,3}$'


class ContactForm(forms.Form):
    fullname = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": 'form-control',
                "id": "form_full_name",
                "placeholder": "Fullname"
            }
        )
    )
    email = forms.EmailField(widget=forms.TextInput(
        attrs={
            "class": "form-control",
            "id": "form_email",
            "placeholder": "Your Email-Id"
        }
    )
    )
    content = forms.CharField(
        widget=forms.TextInput(
            attrs={
                "class": "form-control",
                "id": "form_content",
                "placeholder": "Content",
            }
        )
    )

    def clean_email(self):
        email = self.cleaned_data.get("email")
        if not re.search(regex, email):
            raise forms.ValidationError("Email not valid")

from django.contrib import admin
from django.urls import path, include
from django.conf import settings
from django.contrib.auth.views import LogoutView
from django.views.generic import TemplateView
from django.conf.urls.static import static
from accounts.views import LoginView, RegisterView, logout_view, guest_login_view
from addresses.views import checkout_address_view, checkout_address_use_view
from .views import home_page, about_page, contact_page
from carts.views import cart_api_view
from billing.views import payment_method_view, payment_method_create_view

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', home_page, name='home'),
    path('about/', about_page, name='about'),
    path('contact/', contact_page, name='contact'),
    path('login/', LoginView.as_view(), name='login'),
    path('guest_login/', guest_login_view, name='guest_login'),
    path('checkout/address/create', checkout_address_view,
         name='checkout_address_view'),
    path('checkout/address/use', checkout_address_use_view,
         name='checkout_address_reuse_view'),
    path('logout/', LogoutView.as_view(), name='logout'),
    path('register/', RegisterView.as_view(), name='register'),
    path('api/cart', cart_api_view, name='cart_api_view'),
    path('bootstrap/', TemplateView.as_view(template_name="bootstrap/example.html")),
    path('products/', include("products.urls", namespace="products")),
    path('search/', include("search.urls", namespace="search")),
    path('cart/', include("carts.urls", namespace="cart")),
    path('products/<slug:slug>/', include("products.urls")),
    path('billing/payment-method/', payment_method_view, name='billing-payment-method'),
    path('billing/payment-method/create/', payment_method_create_view, name='billing-payment-method-endpoint')
]

if settings.DEBUG:
    urlpatterns = urlpatterns + \
        static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)

if settings.DEBUG:
    urlpatterns = urlpatterns + \
        static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

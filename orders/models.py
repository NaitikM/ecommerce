from django.db import models
from carts.models import Cart
from django.db.models.signals import post_save, pre_save
from ecommerce.utils import unique_order_id_generator
from billing.models import BillingProfile
from addresses.models import Address
# Create your models here.
order_status_choices = (
    ('created', 'Created'),
    ('paid', 'Paid'),
    ('shipped', 'Shipped'),
    ('refunded', 'Refunded'),
)


class OrderModelManager(models.Manager):
    def new_or_get(self, billing_profile, cart_obj):
        qs = self.get_queryset().filter(
            billing_profile=billing_profile, cart=cart_obj, active=True).exclude(status='paid')
        if qs.exists():
            created = False
            obj = qs.first()
        else:
            created = True
            obj = self.model.objects.create(
                billing_profile=billing_profile, cart=cart_obj)
        return obj, created


class Order(models.Model):
    order_id = models.CharField(
        max_length=120, null=True, blank=True, unique=True)
    billing_profile = models.ForeignKey(BillingProfile, related_name='shipping_address',on_delete=models.CASCADE, null=True, blank=True)
    shipping_address = models.ForeignKey(Address, related_name='billing_address', on_delete=models.CASCADE, null=True, blank=True)
    billing_address = models.ForeignKey(Address, on_delete=models.CASCADE, null=True, blank=True)
    cart = models.ForeignKey(
        Cart, on_delete=models.CASCADE, null=False, blank=False)
    status = models.CharField(
        max_length=120, default='created', choices=order_status_choices)
    shipping_total = models.DecimalField(
        default=5.99, max_digits=100, decimal_places=2)
    total = models.DecimalField(default=0, max_digits=100, decimal_places=2)
    active = models.BooleanField(default=True)
    def __str__(self):
        return self.order_id

    objects = OrderModelManager()

    def update_total(self):
        cart_total = self.cart.total
        shipping_total = self.shipping_total
        total = float(cart_total) + float(shipping_total)
        self.total = total
        self.save()
        return total

    def check_done(self):
        billing_profile = self.billing_profile
        shipping_address = self.shipping_address
        billing_address = self.billing_address
        total  = self.total
        if billing_address and billing_profile and shipping_address and total > 0:
            return True
        return False

    def mark_done(self):
        if self.check_done():
            self.status = 'paid'
            self.save()
        return self.status

# Generate the order id
def pre_save_create_unique_order_id(sender, instance, * args, **kwargs):
    if not instance.order_id:
        instance.order_id = unique_order_id_generator(instance)
    qs = Order.objects.filter(cart=instance.cart).exclude(
        billing_profile=instance.billing_profile)
    if qs.exists():
        qs.update(active=True)


pre_save.connect(pre_save_create_unique_order_id, Order)


# Generate the order total
def post_save_generate_order_total(sender, instance, created, *args, **kwargs):
    if not created:
        cart_obj = instance
        cart_total = cart_obj.total
        cart_id = cart_obj.id
        qs = Order.objects.filter(cart__id=cart_id)
        if qs.exists():
            order_obj = qs.first()
            order_obj.update_total()


post_save.connect(post_save_generate_order_total, Cart)


def post_save_order(sender, instance, created, *args, **kwargs):
    if created:
        instance.update_total()


post_save.connect(post_save_order, Order)

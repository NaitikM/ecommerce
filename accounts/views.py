from django.shortcuts import HttpResponse
from django.shortcuts import render, redirect
from .forms import LoginForm, GuestForm, UserAdminCreationForm
from django.contrib.auth import login, authenticate, get_user_model
from django.contrib.auth import logout
from django.utils.http import is_safe_url
from .models import GuestEmail
from django.views.generic import CreateView, FormView
from .signals import user_logged_in


def guest_login_view(request):
    form = GuestForm(request.POST or None)
    context = {
        "form": form
    }
    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post
    if form.is_valid():
        email = form.cleaned_data.get('email')
        new_guest_email = GuestEmail.objects.create(email=email)
        request.session['guest_email_id'] = new_guest_email.id
        if is_safe_url(redirect_path, request.get_host()):
            return redirect(redirect_path)
        else:
            return redirect("/register")
    else:
        context["error"] = True
    return redirect('/register/')


class LoginView(FormView):
    form_class = LoginForm
    template_name = "auth/login.html"
    success_url = "/"
    context = {}

    def get_context_data(self, *args, **kwargs):
        context = super(LoginView, self).get_context_data(
            **kwargs)
        request = self.request
        return context

    def form_valid(self, form):
        request = self.request
        next_ = request.GET.get('next')
        next_post = request.POST.get('next')
        redirect_path = next_ or next_post
        email = form.cleaned_data.get('email')
        password = form.cleaned_data.get('password')
        user = authenticate(username=email, password=password)
        if user is not None:
            login(request, user)
            user_logged_in.send(sender=user.__class__, instance=user, request=request)
            print("sent")
            try:
                del request.session['guest_email_id']
            except Exception:
                pass
            if is_safe_url(redirect_path, request.get_host()):
                return redirect(redirect_path)
            else:
                return redirect("/")
        return super(LoginView, self).form_valid(form)


class RegisterView(CreateView):
    form_class = UserAdminCreationForm
    template_name = "auth/register.html"
    success_url = "/login"


def logout_view(request):
    logout(request)
    return render(request, "home_page.html", {})

from django.shortcuts import render, redirect
import stripe
from django.conf import settings
from .models import BillingProfile, Card
from django.http import HttpResponse, JsonResponse
from django.utils.http import is_safe_url

STRIPE_PUB_KEY = getattr(
				settings, 
				"STRIPE_PUB_KEY", 
				"pk_test_51GywIzFVXVw963j8TDeI7tKEXGAsG6IioX0oZuJH472UeYBCJ6t7g5iTyJduQbH61bAudVhlnaxRjAUtr9MhDtmq00BI8K0nwN"
				)
stripe.api_key = getattr(
				settings, 
				"STRIPE_SECRET_KEY", 
				"sk_test_51GywIzFVXVw963j8eHPcER77M9HVi0UrEkG5r71qc2XxjYQkdm0ZWfsfh9eZwfNJZQhubdsFncEpUcxwwDHN0Tr000sKfUkEjh"
				)


def payment_method_view(request):
	billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
	if not billing_profile:
		return redirect("/cart")
	next_ = request.GET.get('next')
	next_url = None
	if is_safe_url(next_, request.get_host()):
		next_url = next_
	context = {
	'publish_key': STRIPE_PUB_KEY,
	'next_url':next_url,
	}
	return render(request, "billing/payment-method.html", context=context)


def payment_method_create_view(request):
	if request.method == "POST" and request.is_ajax():
		billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
		if not billing_profile:
			return HttpResponse("Cannot verify user", status=403)
		token = request.POST.get("token")
		if token is not None:
			# customer = stripe.Customer.retrieve(billing_profile.customer_id)
			# card_response = customer.sources.create(source=token)
			new_card_obj = Card.objects.add_new(billing_profile=billing_profile, token=token)
		return JsonResponse({"message":"Success! Your card was added"})
	return HttpResponse("Method not allowed", status=405)

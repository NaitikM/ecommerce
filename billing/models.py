from django.db import models
from django.conf import settings
from django.urls import reverse
from django.db.models.signals import post_save, pre_save
from accounts.models import GuestEmail
import stripe

User = settings.AUTH_USER_MODEL
pvt_key = '''sk_test_51GywIzFVXVw963j8eHPcER77M9HVi0UrEkG5r71qc2XxjYQkdm0ZWfsfh9eZwfNJZQhubdsFncEpUcxwwDHN0Tr000sKfUkEjh'''
stripe.api_key = pvt_key
# Create your models here.


class BillingModelManager(models.Manager):
    def new_or_get(self, request):
        user = request.user
        guest_email_id = request.session.get('guest_email_id')
        created = False
        obj = None
        if user.is_authenticated:
            obj, created = self.model.objects.get_or_create(
                user=user, email=user.email)
        elif guest_email_id is not None:
            email_obj = GuestEmail.objects.get(id=guest_email_id)
            obj, created = self.model.objects.get_or_create(
                user=None, email=email_obj.email)
        else:
            pass
        return obj, created


class BillingProfile(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, null=True, blank=True, unique=True)
    email = models.EmailField()
    active = models.BooleanField(default=True)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)
    customer_id = models.CharField(max_length=255, blank=True, null=True)

    objects = BillingModelManager()

    def __str__(self):
        return self.email

    def charge(self, order_obj, card=None):
        return Charge.objects.do(self, order_obj, card)

    def get_cards(self):
        return self.card_set.all()

    @property
    def has_card(self):
        card_qs = self.get_cards()
        return card_qs.exists()

    @property
    def default_card(self):
        try:
            return self.get_cards().filter(default=True, active=True).first()
        except:
            return None

    def set_cards_inactive(self):
        cards_qs = self.get_cards()
        cards_qs.update(active=False)
        return cards_qs.filter(active=True).count()

    def get_payment_method_url(self):
        return reverse('billing-payment-method')


def user_created_receiver(sender, instance, created, *args, **kwargs):
    if created and instance.email:
        print(instance.email)
        BillingProfile.objects.get_or_create(
            user=instance, email=instance.email)


post_save.connect(user_created_receiver, sender=User)


def billing_profile_created_receiver(sender, instance, *args, **kwargs):
    if not instance.customer_id and instance.email:
        customer = stripe.Customer.create(
            email=instance.email,
        )
        print("API call")
        instance.customer_id = customer.id


pre_save.connect(billing_profile_created_receiver, sender=BillingProfile)



class CardManager(models.Manager):
    def all(self, *args, **kwargs):
        return self.get_queryset().filter(active=True)
    
    def add_new(self, billing_profile, token):
        if token:
            customer = stripe.Customer.retrieve(billing_profile.customer_id)
            # card_qs = self.get_queryset().filter(billing_profile=billing_profile)
            # card_qs.update(default=False)
            stripe_card_response = customer.sources.create(source=token)
            new_card = self.model(
                    billing_profile = billing_profile,
                    stripe_id = stripe_card_response.id,
                    brand = stripe_card_response.brand,
                    country = stripe_card_response.country,
                    expiration_month = stripe_card_response.exp_month,
                    expiration_year = stripe_card_response.exp_year,
                    last4 = stripe_card_response.last4,
                )
            new_card.save()
            return new_card
        return None


class Card(models.Model):
    stripe_id           = models.CharField(max_length=255)
    billing_profile     = models.ForeignKey(BillingProfile, on_delete=models.CASCADE)
    brand               = models.CharField(max_length=120, null=True, blank=True)
    country             = models.CharField(max_length=20, null=True, blank=True)
    expiration_month    = models.IntegerField(null=True, blank=True)
    expiration_year     = models.IntegerField(null=True, blank=True)
    last4               = models.CharField(max_length=4, null=True, blank=True)
    default             = models.BooleanField(default=True)
    active              = models.BooleanField(default=True)
    timestamp           = models.DateTimeField(auto_now_add=True)

    objects = CardManager()

    def __str__(self):
        return f"{self.brand} {self.last4}"


def new_card_created_receiver(sender, instance, created, *args, **kwargs):
    if instance.default:
        billing_profile = instance.billing_profile
        qs = Card.objects.filter(billing_profile=billing_profile).exclude(pk=instance.pk)
        qs.update(default=False)

post_save.connect(new_card_created_receiver, sender=Card)


class ChargeManager(models.Manager):
    def do(self, billing_profile, order_obj, card=None):
        card_obj = card
        if card_obj == None:
            cards = billing_profile.card_set.filter(default=True)
            if cards.exists():
                card_obj = cards.first()
        if card_obj == None:
            return False, "No cards available."

        # charge = stripe.Charge.create(
        #     amount = int(order_obj.total * 100),
        #     currency = "inr",
        #     customer = billing_profile.customer_id,
        #     source  = card_obj.stripe_id,
        #     #metadata = {"order_id" : order_obj.order_id}
        # )
        charge = stripe.Charge.create(
          amount = int(order_obj.total * 100),
          currency = "inr",
          customer = billing_profile.customer_id,
          source  = card_obj.stripe_id,
          description= "Charge for {}".format(billing_profile)
        )
        new_charge_obj = self.model(
            billing_profile = billing_profile,
            stripe_id = charge.stripe_id,
            paid = charge.paid,
            refunded = charge.refunded,
            outcome = charge.outcome,
            outcome_type = charge.outcome.get("type", None),
            seller_message = charge.outcome.get("seller_message", None),
            risk_level = charge.outcome.get("risk_level", None)
        )
        new_charge_obj.save()
        return new_charge_obj.paid, new_charge_obj.seller_message

class Charge(models.Model):
    stripe_id = models.CharField(max_length=255)
    billing_profile = models.ForeignKey(BillingProfile, on_delete=models.CASCADE)
    paid = models.BooleanField(default=False)
    refunded = models.BooleanField(default=False)
    outcome = models.TextField(null=True, blank=True)
    outcome_type = models.CharField(max_length=120, null=True, blank=True)
    seller_message = models.CharField(max_length=120, null=True, blank=True)
    risk_level = models.CharField(max_length=120, null=True, blank=True)

    objects = ChargeManager()









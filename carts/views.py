from django.shortcuts import render, redirect
from .models import Cart
import stripe
from django.conf import settings
from django.http import JsonResponse
from addresses.forms import AddressForm
from addresses.models import Address
from accounts.models import GuestEmail
from products.models import Product
from orders.models import Order
from accounts.forms import LoginForm, GuestForm
from billing.models import BillingProfile


STRIPE_PUB_KEY = getattr(
                settings, 
                "STRIPE_PUB_KEY", 
                "pk_test_51GywIzFVXVw963j8TDeI7tKEXGAsG6IioX0oZuJH472UeYBCJ6t7g5iTyJduQbH61bAudVhlnaxRjAUtr9MhDtmq00BI8K0nwN"
                )
stripe.api_key = getattr(
                settings, 
                "STRIPE_SECRET_KEY", 
                "sk_test_51GywIzFVXVw963j8eHPcER77M9HVi0UrEkG5r71qc2XxjYQkdm0ZWfsfh9eZwfNJZQhubdsFncEpUcxwwDHN0Tr000sKfUkEjh"
                )


def cart_api_view(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    products = [{"name": product.title,
                 "price": product.price,
                 "url": product.get_absolute_url(),
                 "id": product.id}
                for product in cart_obj.products.all()]
    cart_data = {
        "products": products,
        "subtotal": cart_obj.subtotal,
        "total": cart_obj.total,
    }
    return JsonResponse(cart_data)


def cart_home(request):
    cart_obj, new_obj = Cart.objects.new_or_get(request)
    try:
        request.session["cart_items"] = cart_obj.products.count()
    except:
        request.session["cart_items"] = 0
    return render(request, "carts/home.html", {"cart": cart_obj})


def cart_update(request):
    if request.POST.get('product_id'):
        obj = Product.objects.get(id=request.POST.get('product_id'))
        cart_obj, new_obj = Cart.objects.new_or_get(request)
        if obj in cart_obj.products.all():
            cart_obj.products.remove(obj)
            added = False
        else:
            cart_obj.products.add(obj)
            added = True
        try:
            request.session["cart_items"] = cart_obj.products.count()
        except:
            request.session["cart_items"] = 0
        if request.is_ajax():
            json_data = {
                "added": added,
                "removed": not added,
                "itemCount": request.session["cart_items"]
            }
            return JsonResponse(json_data, status=200)
    return redirect("cart:home")


def checkout_home(request):
    cart_obj, new_cart = Cart.objects.new_or_get(request)
    order_obj = None
    login_form = LoginForm()
    guest_form = GuestForm()
    guest_email_id = request.session.get('guest_email_id')
    address_form = AddressForm()
    billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(
        request)
    billing_address_id = request.session.get('billing_address_id', None)
    shipping_address_id = request.session.get('shipping_address_id', None)
    address_qs = None
    has_card = False
    if billing_profile is not None:
        if request.user.is_authenticated:
            address_qs = Address.objects.filter(
                billing_profile=billing_profile)
        # shipping_address_qs = address_qs.filter(address_type='shipping')
        # billing_address_qs = address_qs.filter(address_type='billing')
        order_obj, order_obj_created = Order.objects.new_or_get(
            billing_profile, cart_obj)
        if shipping_address_id:
            order_obj.shipping_address = Address.objects.get(
                id=shipping_address_id)
            del request.session['shipping_address_id']
        if billing_address_id:
            order_obj.billing_address = Address.objects.get(
                id=billing_address_id)
            del request.session['billing_address_id']
        if billing_address_id or shipping_address_id:
            order_obj.save()
        has_card = billing_profile.has_card

    if request.method == "POST":
        is_prepared = order_obj.check_done()
        if is_prepared:
            did_charge, charge_msg = billing_profile.charge(order_obj)
            if did_charge:
                order_obj.mark_done()
                del request.session["cart_id"]
                if not billing_profile.user:
                    billing_profile.set_cards_inactive()
                request.session['cart_items'] = 0
                return redirect("cart:checkout_complete")
            else:
                print(charge_msg)
        return redirect("cart:checkout")
    try:
        request.session["cart_items"] = cart_obj.products.count()
    except:
        request.session["cart_items"] = 0
    context = {
        "billing_profile": billing_profile,
        "order": order_obj,
        "cart": cart_obj,
        "form": login_form,
        "guest_form": guest_form,
        "address_form": address_form,
        'address_qs': address_qs,
        "has_card":has_card,
        "publish_key": STRIPE_PUB_KEY ,
    }
    return render(request, 'carts/checkout.html', context)


def checkout_complete(request):
    context = {}
    return render(request, 'carts/checkout_done.html', context)

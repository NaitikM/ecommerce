from django.db import models
from django.conf import settings
from products.models import Product
from django.db.models.signals import pre_save, post_save, m2m_changed
User = settings.AUTH_USER_MODEL
# Create your models here.


class CartManager(models.Manager):

    def new_or_get(self, request):
        cart_id = request.session.get("cart_id", None)
        qs = self.get_queryset().filter(id=cart_id)
        if qs.count() == 1:
            cart_obj = qs.first()
            if request.user.is_authenticated and cart_obj.user is None:
                cart_obj.user = request.user
                cart_obj.save()
            new_obj = False
        else:
            cart_obj = Cart.objects.new_cart(user=request.user)
            request.session["cart_id"] = cart_obj.id
            new_obj = True
        return cart_obj, new_obj

    def new_cart(self, user=None):
        user_object = None
        if user is not None:
            if user.is_authenticated:
                user_object = user
        return self.model.objects.create(user=user_object)


class Cart(models.Model):
    user = models.ForeignKey(
        User, null=True, blank=True, on_delete=models.CASCADE)
    products = models.ManyToManyField(Product, null=True, blank=True)
    subtotal = models.DecimalField(
        max_digits=11, default=0.00, decimal_places=2)
    total = models.DecimalField(max_digits=11, default=0.00, decimal_places=2)
    timestamp = models.DateTimeField(auto_now_add=True)
    updated = models.DateTimeField(auto_now_add=True)

    objects = CartManager()

    def __str__(self):
        return str(self.id)


def pre_save_cart_receiver(sender, instance, action, *args, **kwargs):
    total = 0
    products = instance.products.all()
    if action == "post_add" or action == "post_remove" or action == "post_clear":
        for x in products:
            total += x.price
        print(total)
        instance.subtotal = float(total)
        instance.total = float(total) * 1.18
        instance.save()


m2m_changed.connect(pre_save_cart_receiver, sender=Cart.products.through)

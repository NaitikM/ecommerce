from django.shortcuts import render, redirect
from .forms import AddressForm
from django.utils.http import is_safe_url
from billing.models import BillingProfile
from .models import Address

# Create your views here.


def checkout_address_view(request):
    form = AddressForm(request.POST or None)
    context = {
        "form": form
    }
    next_ = request.GET.get('next')
    next_post = request.POST.get('next')
    redirect_path = next_ or next_post
    if form.is_valid():
        billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
        instance = form.save(commit=False)
        if billing_profile is not None:
            address_type = request.POST.get('address_type', 'shipping')
            print(request.POST)
            instance.billing_profile = billing_profile
            instance.address_type = request.POST.get('address_type', 'shipping')
            instance.save()
            print(address_type)
            request.session[address_type + "_address_id"] = instance.id
        else:
            redirect("cart:checkout")
        if is_safe_url(redirect_path, request.get_host()):
            return redirect(redirect_path)
    return redirect('/register/')


def checkout_address_use_view(request):
    if request.user.is_authenticated:
        next_ = request.GET.get('next')
        next_post = request.POST.get('next')
        redirect_path = next_ or next_post
        if request.method == "POST":
            print(request.POST)
            shipping_address = request.POST.get('shipping_address', None)
            billing_profile, billing_profile_created = BillingProfile.objects.new_or_get(request)
            address_type = request.POST.get('address_type', 'shipping')
            qs = Address.objects.filter(billing_profile=billing_profile)
            if qs.exists():
                request.session[address_type + "_address_id"] = shipping_address
            if is_safe_url(redirect_path, request.get_host()):
                return redirect(redirect_path)
    else:
        redirect("cart:checkout")
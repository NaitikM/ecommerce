$(document).ready(function() {
  function showErrorAlert(msg) {
    $.alert({
      title: 'Oops!',
      content: msg || 'An error occured!',
      type: 'red',
      theme: 'modern',
      icon: 'fa fa-exclamation',
      closeIcon: true,
      animation: 'zoom',
      draggable: false,
    });
  }

  function showSuccessAlert(data) {
    $.alert({
      title: 'Success',
      content: data.message,
      type: 'green',
      theme: 'modern',
      icon: 'fa fa-circle',
      closeIcon: true,
      animation: 'zoom',
      draggable: false
    });
  }
  var contactForm = $('.contact-form')
  var contactFormMethod = contactForm.attr('method')
  var contactFormEndpoint = contactForm.attr('action')
  var contactFormSumbitBtn = contactForm.find("[type='submit']")
  var contactFormSumbitBtnTxt = contactFormSumbitBtn.text()

  function showSumbit(doSubmit) {
    if (doSubmit) {
      contactFormSumbitBtn.addClass('disabled')
      contactFormSumbitBtn.html("<i class='fas fa-spinner fa-spin'></i> Sending")
    } else {
      contactFormSumbitBtn.removeClass('disabled')
      contactFormSumbitBtn.html(contactFormSumbitBtnTxt)
    }
  }
  contactForm.submit(function(event) {
    event.preventDefault()
    var contactFormData = contactForm.serialize()
    var thisForm = $(this)
    showSumbit(true)
    $.ajax({
      url: contactFormEndpoint,
      method: contactFormMethod,
      data: contactFormData,
      success: function(data) {
        thisForm[0].reset()
        setTimeout(function() {
          showSumbit(false)
        }, 1000)
        setTimeout(function() {
          showSuccessAlert(data)
        }, 500)
      },
      error: function(errorData) {
        var json_data = errorData.responseJSON
        var msg = ""
        $.each(json_data, function(key, value) {
          msg += key.charAt(0).toUpperCase() + key.slice(1) + " : " + value[0].message + "<br />"
        })
        setTimeout(function() {
          showSumbit(false)
        }, 500)
        setTimeout(function() {
          showErrorAlert(msg)
        }, 500)
      }
    })
  })
  var searchForm = $('.search-form')
  var searchInput = searchForm.find("[name='q']")
  var typingTimer;
  var typingInterval = 800;
  var searchBtn = searchForm.find("[type='submit']")
  searchInput.keyup(function(event) {
    clearTimeout(typingTimer)
    typingTimer = setTimeout(performSearch, typingInterval)
  })
  searchInput.keydown(function() {
    clearTimeout(typingTimer)
  })

  function showLoading() {
    searchBtn.addClass('disabled')
    searchBtn.html("<i class='fas fa-spinner fa-spin'></i> Searching")
  }
  // function showNormal(){
  //   searchBtn.removeClass('disabled')
  //   searchBtn.html('<button class="btn btn-success" type="submit">Search</button>')
  // }
  function performSearch() {
    var query = searchInput.val()
    if (query.length > 0) {
      showLoading()
      setTimeout(function() {
        window.location.href = '/search/?q=' + query
      }, 500)
    } else {
      console.log("Blank search");
    }
  }

  var productForm = $('.form-product-ajax');
  productForm.submit(function(event) {
    event.preventDefault();
    var thisForm = $(this)
    //var actionEndpoint = thisForm.attr('action');
    var actionEndpoint = thisForm.attr('data-endpoint');
    var httpMethod = thisForm.attr('method');
    var formData = thisForm.serialize();

    $.ajax({
      url: actionEndpoint,
      method: httpMethod,
      data: formData,
      success: function(data) {
        var submitSpan = thisForm.find(".submit-span")
        if (data.added) {
          submitSpan.html('<button class="btn btn-danger" type="submit">Remove</button>')
        } else {
          submitSpan.html('<button class="btn btn-success" type="submit">Add to Cart</button>')
        }
        var navbarCount = $('.navbar_item_count')
        navbarCount.text(data.itemCount)
        var currentPath = window.location.href

        if (currentPath.indexOf("cart") != -1) {
          refreshCart()
        }
      },
      error: function(errorData) {
        console.log("error", errorData);
        showErrorAlert()
      }
    })
  })

  function refreshCart() {
    var cartTable = $(".cart-table")
    var cartBody = cartTable.find(".cart-body")
    var productRows = cartBody.find(".cart-product")
    var currentUrl = window.location.href
    var refreshCartUrl = '/api/cart';
    var refreshCartMethod = "GET";
    var refreshCartData = {};
    $.ajax({
      url: refreshCartUrl,
      method: refreshCartMethod,
      data: refreshCartData,

      success: function(data) {
        var hiddenCartItemRemoveForm = $('.cart-item-remove-form')
        if (data.products.length > 0) {
          productRows.html("")
          i = data.products.length
          $.each(data.products, function(index, value) {
            var newCartItem = hiddenCartItemRemoveForm.clone()
            newCartItem.removeClass("display")
            newCartItem.find(".cart-item-product-id").val(value.id)
            cartBody.prepend("<tr><th scope=\"row\">" + i + "</th><td><a href='" + value.url + "'>" +
              value.name +
              "</a></td><td>" +
              value.price +
              "</td><td>" +
              newCartItem.html() +
              "</td></tr>")
            i--
          });
          cartBody.find(".cart-subtotal").text(data.subtotal)
          cartBody.find(".cart-total").text(data.total)
        } else {
          window.location.href = currentUrl
        }
      },
      error: function(errorData) {
        console.log("error", errorData);
        showErrorAlert()
      }
    })
  }
})
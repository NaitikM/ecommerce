$(document).ready(function (){
	var stripeFormModule=$(".stripe-payment-form")
	var stripeTemplate = $.templates('#stripeTemplate')
	var stripeModuleToken = stripeFormModule.attr('data-token')
	var stripeModuleBtnTitle = stripeFormModule.attr('data-btn-title') || "Add card"
	var stripeModuleNextUrl = stripeFormModule.attr('data-next-url')

	var stripeTemplateDataContext = {
		publishKey: stripeModuleToken,
		nextUrl: stripeModuleNextUrl,
		btnTitle:stripeModuleBtnTitle
	}
	var stripeTemplateHtml = stripeTemplate.render(stripeTemplateDataContext)
	stripeFormModule.html(stripeTemplateHtml)
	var paymentForm = $('.payment-form')
	if (paymentForm.length > 1){
		$.alert({
	      title: 'Error!',
	      content:'You can\'t submit more than one form',
	      type: 'red',
	      theme: 'modern',
	      icon: 'fa fa-exclamation',
	      closeIcon: true,
	      animation: 'zoom',
	      draggable: false,
	    });
	    paymentForm.css('display', 'none')
	}
	else if (paymentForm.length == 1){
		var pubKey = paymentForm.attr('data-token')
		var nextUrl = paymentForm.attr('data-next-url')
		var stripe = Stripe(pubKey);
		var elements = stripe.elements();
		// Custom styling can be passed to options when creating an Element.
		var style = {
		  base: {
		    // Add your base input styles here. For example:
		    fontSize: '16px',
		    color: '#32325d',
		  },
		};

		// Create an instance of the card Element.
		var card = elements.create('card', {style: style});
		var cardClone = card

		// Add an instance of the card Element into the `card-element` <div>.
		card.mount('#card-element');
		// Create a token or display an error when the form is submitted.
		// var form = document.getElementById('payment-form');
		// form.addEventListener('submit', function(event) {
		//   event.preventDefault();
		//   var loadTime = 1500
		//   var errorHtml = "<i class='fa fa-warning'></i> An error occured."
		//   var errorClasses = "btn btn-danger my-2 disabled"
		//   var loadingHtml = "<i class='fa fa-spin fa-spinner mx-auto'></i> Loading."
		//   var loadingClasses = "btn btn-success my-2 disabled"
		//   stripe.createToken(card).then(function(result) {
		//     if (result.error) {
		//       // Inform the customer that there was an error.
		//       // var errorElement = document.getElementById('card-errors');
		//       // errorElement.textContent = result.error.message;
		// 	      $.alert({
		//       title: 'Error!',
		//       content: result.error.message || 'Error. Failed to add payment method',
		//       type: 'red',
		//       theme: 'modern',
		//       icon: 'fa fa-exclamation',
		//       closeIcon: true,
		//       animation: 'zoom',
		//       draggable: false,
		//     });
		//     } else {
		//       // Send the token to your server.
		//       stripeTokenHandler(result.token);
		//     }
		//   });
		// });
		var form = $('#payment-form');
		form.on('submit', function(event) {
		  event.preventDefault();
		  var btnLoad = form.find('.btn-load')
		  btnLoad.blur()
		  var loadTime = 1500
		  var errorHtml = "<i class='fa fa-warning'></i> An error occured."
		  var errorClasses = "btn btn-danger my-2 disabled"
		  var loadingHtml = "<i class='fa fa-spin fa-spinner mx-auto'></i> Loading."
		  var loadingClasses = "btn btn-success my-2 disabled"
		  currentTimeout = null
		  stripe.createToken(card).then(function(result) {
		    if (result.error) {
		      // Inform the customer that there was an error.
		      // var errorElement = document.getElementById('card-errors');
		      // errorElement.textContent = result.error.message;
			    $.alert({
			      title: 'Error!',
			      content: result.error.message || 'Error. Failed to add payment method',
			      type: 'red',
			      theme: 'modern',
			      icon: 'fa fa-exclamation',
			      closeIcon: true,
			      animation: 'zoom',
			      draggable: false,
			    });
			    currentTimeout = displayBtnStatus(btnLoad, errorHtml, errorClasses, loadTime, currentTimeout)

		    } else {
		      // Send the token to your server.
		      currentTimeout = displayBtnStatus(btnLoad, loadingHtml, loadingClasses, 2000, currentTimeout)
		      stripeTokenHandler(result.token);
		    }
		  });
		});
		//display status
		function displayBtnStatus(element, newHtml, newClasses, loadTime, timeout){
			// if (timeout){
			// 	clearTimeout(timeout)
			// }
			if (!loadTime){
				loadTime = 1500
			}
			var defaultHtml = element.html()
			var defaultClasses = element.attr("class")
			element.html(newHtml)
			element.removeClass(defaultClasses)
			element.addClass(newClasses)
			return setTimeout(function(){
				element.html(defaultHtml)
				element.removeClass(newClasses)
				element.addClass(defaultClasses)
			}, loadTime)
		}

		//redirect to next
		function redirectToNext(nextPath, timeout){
			if(nextPath){
	    		setTimeout(function(){
	    			window.location.href = nextPath
	    		}, timeout)
			}
		}
		// handle stripe token
		function stripeTokenHandler(token){
			var paymentFormEndpoint = "/billing/payment-method/create/"
			var data_ajax = {
				"token": token.id
			}
			$.ajax({
				data: data_ajax,
				url: paymentFormEndpoint,
				method: "POST",
				success: function(data){
					var successMsg = data.message || 'Success! Your card was added'
					card.clear()
					if(nextUrl){
						successMsg += "<br/><i class='fa fa-spin fa-spinner'></i>Redirecting to billing page..."
					}
					if($.alert){
						$.alert({
					      title: 'Sucecess!',
					      content: successMsg,
					      type: 'green',
					      theme: 'modern',
					      icon: 'fa fa-exclamation',
					      closeIcon: false,
					      animation: 'zoom',
					      draggable: false,
					      button:false,
					      confirmButton:false
					    });
					var loadingHtml = "<i class='fa fa-spin fa-spinner mx-auto'></i> Loading."
		  			var loadingClasses = "btn btn-success my-2 disabled"
		  			var btnLoad = form.find('.btn-load')
					btnLoad.html(loadingHtml)
					btnLoad.addClass(loadingClasses)
					}
					else{
						alert(successMsg)
					}
					redirectToNext(nextUrl, 1000)
				},
				error: function(error){
					$.alert({
				      title: 'Error!',
				      content: 'Error. Failed to add payment method',
				      type: 'red',
				      theme: 'modern',
				      icon: 'fa fa-exclamation',
				      closeIcon: true,
				      animation: 'zoom',
				      draggable: false,
				    });
				}
			})
		}
	}
	else if (paymentForm.length < 0){
		$.alert({
	      title: 'Error!',
	      content:'You can\'t submit',
	      type: 'red',
	      theme: 'modern',
	      icon: 'fa fa-exclamation',
	      closeIcon: true,
	      animation: 'zoom',
	      draggable: false,
	    });
	    paymentForm.css('display', 'none')
	}
})
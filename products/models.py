from django.db import models
import random
import os
from django.db.models.signals import pre_save, post_save
from ecommerce.utils import unique_slug_generator
from django.urls import reverse
from django.db.models import Q
# Create your models here.

def get_filename_ext(filename):
    base_name = os.path.basename(filename)
    name,ext  = os.path.splitext(filename)
    return name, ext

def upload_image_path(instance, filename):
    print(instance)
    print(filename)
    new_filename = random.randint(100000000,99999999999999999)
    name, ext = get_filename_ext(filename)
    final_filename = '{new_filename}{ext}'.format(new_filename=new_filename, ext=ext)
    return "products/{new_filename}{final_filename}".format(new_filename=new_filename, final_filename=final_filename)

class ProductQueryset(models.query.QuerySet):
    def active(self):
        return self.filter(active= True)

    def featured(self):
        return self.filter(active= True, featured= True)

    def search(self, query):
        lookups = ( Q(title__icontains=query)|
                    Q(description__icontains=query)|
                    Q(price__icontains=query)|
                    Q(tag__title__icontains=query))
        return self.filter(lookups).distinct()


class ProductManager(models.Manager):

    def get_queryset(self):
        return ProductQueryset(self.model, using= self._db)

    def get_by_id(self, pk):
        qs = Product.objects.filter(id=pk)
        if qs.count() == 1:
            return qs.first()
        return None

    def get_featured_products(self):
        return self.get_queryset().featured()

    def all(self):
        return self.get_queryset().active()

    def search(self, query):
        return self.get_queryset().active().search(query)


class Product(models.Model):
    title       = models.CharField(max_length=120)
    description = models.TextField()
    price       = models.DecimalField(decimal_places=2, max_digits=10, default=0.00)
    image       = models.ImageField(upload_to=upload_image_path, blank=True, null=True)
    featured    = models.BooleanField(default = False)
    active      = models.BooleanField(default = True)
    slug        = models.SlugField(blank=True, unique=True)
    timestamp   = models.DateTimeField(auto_now_add=True)
    objects = ProductManager()

    def get_absolute_url(self):
        return reverse("products:detail", kwargs={"slug":self.slug})

    def __str__(self):
        return self.title

def product_pre_save_receiver(sender, instance, *args, **kwargs):
    if not instance.slug:
        instance.slug = unique_slug_generator(instance)

pre_save.connect(product_pre_save_receiver, sender=Product)
